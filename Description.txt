Bengaluru's Favorite Car Color

In a world full of colors, a car’s color may seem like one of the least important decisions when you’re in the market for a new ride. But it’s not as frivolous a concern as it appears. Our assignment focusus on determining the most favorite color of the car of the people of Bengaluru.

As a starter, we began clicking pictures for our assignment. Once we had reasonable number of pictures, we researched about OpenCV, and its methods.
We learnt about morphological transformations like erosion, dilation,etc  and how to isolating an image. We were able to obtain a cropped car put on a black background.

Once we were done with that, we focussed on determining the color of the car. We used KMeans clustering with k=3. To visualize, we 
plotted histogram and generalized the fact that the second most dominant color was the required color of the car. A python script was written and we were able to obtain the rgb value of the cars' color.

We decided to categorize our cars into 8 categories - Black, White, Blue, Green, Yellow, Orange, Red and Grey. Using Euclid's distance method, we calulated the minimum distance and accordingly put the car into the respective category.

After this, we generated a text file with imagename and corrsponding color. After running a hadoop mapreduce program on this, we conclude that grey was the most occuring color.

